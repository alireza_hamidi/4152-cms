function postTo(path, data) {
    var postData = JSON.stringify(data);

    var outData;
    $.ajax({
        url: path,
        data: { data: postData },
        method: 'POST',
        async: false,
        success: function (data) {
            outData = data;
        }
    });
    return outData;
}

function getTo(path) {
    try {
        var data = {};
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", path, false);
        xmlhttp.send();
        if (xmlhttp.status === 200) {
            return xmlhttp.responseText;
        }
    } catch (err) {
        console.log(err);
    }
}

function showAlert(header_, text_, buttons) {
    var alert = document.getElementById("alert");
    var header = document.getElementById("alert-header");
    var text = document.getElementById("alert-text");
    var footer = document.getElementById("modal-footer");

    footer.innerHTML = "";
    header.innerText = header_;
    text.innerText = text_;
    buttons.forEach(function (element) {

        var classes = " ";
        if (element.classes && element.classes.length > 0)
            element.classes.forEach(function (clas) {
                classes += (clas + " ");
            }, this);
        footer.innerHTML += '<button class="modal-action waves-effect waves-green btn-flat' + classes + '" onclick="' + element.onclick + '">' + element.name + '</button>'
    }, this);

    $("#alert").modal('open');
}

function showDesignedAlert(header_, html_, buttons) {
    var alert = document.getElementById("alert");
    var header = document.getElementById("alert-header");
    var html = document.getElementById("alert-html");
    var footer = document.getElementById("modal-footer");

    footer.innerHTML = "";
    header.innerText = header_;
    html.innerHTML = html_;
    buttons.forEach(function (element) {

        var classes = " ";
        if (element.classes && element.classes.length > 0)
            element.classes.forEach(function (clas) {
                classes += (clas + " ");
            }, this);
        footer.innerHTML += '<button class="modal-action waves-effect waves-green btn-flat' + classes + '" onclick="' + element.onclick + '">' + element.name + '</button>'
    }, this);

    $("#alert").modal('open');
}

function logger(text) {
    console.log(text);
}

function redirect(url) {
    window.location.replace(url);
}

function get(path) {
    try {
        var data = {};
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", path, false);
        xmlhttp.send();
        if (xmlhttp.status === 200) {
            return xmlhttp.responseText;
        }
    } catch (err) {
        console.log(err);
    }
}

function loadin(path) {
    console.log("PATH : ", path);
    var compnent = get("components/info.html");
    var element = document.getElementById("loadin");

    var opacity = 1;
    var interv = setInterval(function () {
        opacity -= 0.01;
        if (opacity >= 0) {
            element.style.opacity = opacity;
        } else {
            clearInterval(interv);
            document.getElementById("loadin").innerHTML = compnent;
            interv = setInterval(function () {
                opacity += 0.01;
                if (opacity <= 1) {
                    element.style.opacity = opacity;
                } else
                    clearInterval(interv);
            }, 20);
        }
    }, 20);

}

function validate(type) {
    if (type == "username") {
        var username = document.getElementById("username");
        if (username.value && username.value.length > 5) {
            username.classList.remove("invalid");
            username.classList.add("valid");
        } else {
            username.classList.remove("valid");
            username.classList.add("invalid");
        }
    }
    if (type == "password") {
        var password = document.getElementById("password");
        if (password.value && password.value.length >= 6) {
            password.classList.remove("invalid");
            password.classList.add("valid");
        } else {
            password.classList.remove("valid");
            password.classList.add("invalid");
        }
    }
    if (type == "repassword") {
        var password = document.getElementById("password");
        var repassword = document.getElementById("repassword");
        if (password.value && password.value.length >= 6 && password.value == repassword.value) {
            repassword.classList.remove("invalid");
            repassword.classList.add("valid");
        } else {
            repassword.classList.remove("valid");
            repassword.classList.add("invalid");
        }
    }
    if (type == "email") {
        var email = document.getElementById("email");
        if ((new RegExp(/\w+[@]\w+[.]\w+/g).test(email.value))) {
            email.classList.remove("invalid");
            email.classList.add("valid");
        } else {
            email.classList.remove("valid");
            email.classList.add("invalid");
        }
    }

    if (type == "encryption") {
        var checkBox = document.getElementById("encrypttion_confirm");
        if (checkBox.checked)
            document.getElementById("encrypt_pass").disabled = false;
        else
            document.getElementById("encrypt_pass").disabled = true;
    }
}

function generalValidate(id, len) {
    var element = document.getElementById(id);

    if (element.value && element.value.length >= len) {
        element.classList.remove("invalid");
        element.classList.add("valid");
    } else {
        element.classList.remove("valid");
        element.classList.add("invalid");
    }
}